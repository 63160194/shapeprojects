/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nakamol.shape;

/**
 *
 * @author OS
 */
public class Rectangle extends Shape {

    private double wide;
    private double length;

    public Rectangle(double wide, double length) {
        super("Rectanger");
        this.wide = wide;
        this.length = length;
    }

    @Override
    public double calArea() {
        if (wide == 0 || length == 0) {
            System.out.println("wide or length must more than zero!!!!");
        }
        return wide * length;
    }

    @Override
    public String toString() {
        return "Area of Rectangle" + "wide = " + wide + ", length = " + length + " is =  " + this.calArea();
    }

}
