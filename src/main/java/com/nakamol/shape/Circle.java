/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nakamol.shape;

/**
 *
 * @author OS
 */
public class Circle extends Shape {

    private double r;
    private static final double PI = 22.0 / 7;

    public Circle(double r) {
        super("Circle");
        this.r = r;

    }

    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }

    @Override
    public double calArea() {
        if (r <= 0) {
            System.out.println("Error : Radius must more than zero!!!!");
        }
        return PI * r * r;
    }

    @Override
    public String toString() {
        return "Area of Circle " + "Radius = " + this.getR() + " is =  " + this.calArea();
    }

}
